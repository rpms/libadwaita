%global apiver  1
%global versuf  alpha.3

Name:           libadwaita
Version:        1.0.0
Release:        0.5.%{versuf}%{?dist}
Summary:        Building blocks for modern GNOME applications

License:        LGPLv2+
URL:            https://gitlab.gnome.org/GNOME/libadwaita
Source0:        %{url}/-/archive/%{version}.%{versuf}/libadwaita-%{version}.%{versuf}.tar.gz

BuildRequires:  gcc
BuildRequires:  gi-docgen
BuildRequires:  intltool
BuildRequires:  meson >= 0.53.0
BuildRequires:  sassc
BuildRequires:  vala

BuildRequires:  pkgconfig(gobject-introspection-1.0)
BuildRequires:  pkgconfig(gtk4)

%description
Building blocks for modern GNOME applications.


%package        devel
Summary:        Development files for %{name}

Requires:       %{name}%{?_isa} = %{version}-%{release}
Requires:       vala
Recommends:     %{name}-doc = %{version}-%{release}

%description    devel
Development files for %{name}.


%package        doc
Summary:        Documentation files for %{name}
BuildArch:      noarch

Recommends:     %{name}-devel = %{version}-%{release}

%description    doc
Documentation files for %{name}.


%prep
%autosetup -n %{name}-%{version}.%{versuf} -p1


%build
%meson \
    -Dgtk_doc=true \
    %{nil}
%meson_build


%install
%meson_install
%find_lang %{name}


%files -f %{name}.lang
%license COPYING
%doc README.md AUTHORS NEWS
%{_bindir}/adwaita-%{apiver}-demo
%{_libdir}/*-%{apiver}.so.0*
%{_libdir}/girepository-1.0/*.typelib
%{_libdir}/gtk-4.0/inspector/*%{apiver}.so.0*

%files devel
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/*-%{apiver}.gir
%{_datadir}/vala/vapi/%{name}-%{apiver}.*
%{_includedir}/%{name}-%{apiver}/
%{_libdir}/*-%{apiver}.so
%{_libdir}/gtk-4.0/inspector/*%{apiver}.so
%{_libdir}/pkgconfig/*-%{apiver}.pc

%files doc
%doc HACKING.md
%{_docdir}/%{name}-%{apiver}/


%changelog
* Fri Oct 01 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 1.0.0-0.5.alpha.3
- chore(update): 1.0.0-0.5.alpha.3

* Mon Aug 30 2021 Lyes Saadi <fedora@lyes.eu> - 1.0.0-0.4.alpha.2
- Updating to alpha.2

* Thu Jun 24 2021 Artem Polishchuk <ego.cordatus@gmail.com> - 1.0.0-0.3.alpha.1
- Initial package